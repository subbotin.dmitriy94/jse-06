package com.tsconsulting.dsubbotin.tm;

import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        run(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
        }
    }

    private static void run(final String... args) {
        if (args == null || args.length == 0) return;
        final String param = args[0];
        switch (param) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                displayError();
                break;
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayAbout() {
        System.out.println("Developer: Dmitriy Subbotin");
        System.out.println("E-Mail: dsubbotin@tsconsulting.com");
    }

    private static void displayVersion() {
        System.out.println("1.6.1");
    }

    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Exit application.");
    }

    private static void displayError() {
        System.out.println("Command not found!");
    }

    private static void exit() {
        System.exit(0);
    }

}
